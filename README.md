# JIRA Issue Intake Form

## About this Repository

This respository contains the source files for a prototype web form and a deployable Google Cloud function. A requestor can submit an adhoc request via the web form to the Cloud Function which will post an issue task type to JIRA Rest API for a specified JIRA Software project.

## Solutions Overview

![alt text](/img/jira-issue-intake-form.png)

## Development

### Requirements
### Getting Started
### Testing

## Documentation

## Support and Feedback

## How to contribute

## Licensing

