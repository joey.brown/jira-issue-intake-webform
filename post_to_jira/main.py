from flask import Flask, request
import os
import requests
from requests.auth import HTTPBasicAuth
import json

# assign constants
JIRA_USERNAME = os.environ.get('JIRA_USERNAME', 'JIRA Username environment variable is not set.')
JIRA_TOKEN = os.environ.get('JIRA_TOKEN', 'JIRA Token environment variable is not set.')
JIRA_PROJECT_API_URL = os.environ.get('JIRA_PROJECT_API_URL', 'JIRA Project URL environment variable is not set.')



# create Flask application instance with name app
app = Flask(__name__)

# decorator that turns python function into a Flask view function, which 
# converst the function's return value into an HTTP response to be displayed
# by an HTTP client (browser)

@app.route('/') # responds on main URL


# valid returned data
def return_values(request):
    return (request.values)

# acquire form data
def post_to_jira(request):
  # check for POST method execution
  if request.method == "POST":
    data = request.form
  
    name = data['name']
    email = data['email']
    phone = data['phone']
    gcp_project = data['gcp_project']
    environment = data['environment']
    gitlab_repo = data['gitlab_repo']
    summary = data['summary']
    description = data['description']
    affected_users = data['affected_users']
    gs_case_number = data['gs_case_number']
    snow_ticket_number = data['snow_ticket_number']
  
  url = f"https://{JIRA_PROJECT_API_URL}/rest/api/3/issue"

  auth = HTTPBasicAuth(JIRA_USERNAME, JIRA_TOKEN)

  headers = {
    "Accept": "application/json",
    "Content-Type": "application/json"
  }

  issue = {
    "update": {},
    "fields": {
      "summary": summary,
      "issuetype": {
          "id": "10002"
      },
      "project": {
          "id": "10000"
      },
      "description": {
        "type": "doc",
        "version": 1,
        "content": [
          {
            "type": "paragraph",
            "content": [
              {
                "text": description,
                "type": "text"
              }
            ]
          }
        ]
      },
      "customfield_10029": email,
      "customfield_10030": name
    }
  }

  payload = json.dumps(issue)

  response = requests.request(
    "POST",
    url,
    data=payload,
    headers=headers,
    auth=auth
    # verify=False
  )
  
  # print(json.dumps(json.loads(response.text), sort_keys=True, indent=4, separators=(",", ": ")))
  
  return (request.values)
